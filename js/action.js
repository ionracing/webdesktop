function WindowDrag(e)
{
    if (dragging)
    {
        draggingObj.style.left = (e.clientX - startX + windowX).toString() + "px";
        draggingObj.style.top = (e.clientY - startY + windowY).toString() + "px";
    }
    else if (resizing)
    {
        draggingObj.style.width = (e.clientX - startX + windowX).toString() + "px";
        draggingObj.style.height = (e.clientY - startY + windowY).toString() + "px";
    }
    else if(draggingObj != null)
    {
        
        draggingObj = null;
    }
}

function dragInit(e) 
{
    dragging = true;
    //var handle = this.handle;
    //var element = handle.handle;
    var obj = this.parentElement;
    //var obj = element;
    if (this.handle.state == MAXIMIZED)
    {
        this.handle.y = e.clientY - 20;
        this.handle.x = e.clientX - this.handle.width / 2;
        this.handle.maximize();
    }
    makeTopMost(obj);
    windowX = obj.offsetLeft;
    windowY = obj.offsetTop;
    startX = e.clientX;
    startY = e.clientY;
    draggingObj = obj;

    this.parentElement.getElementsByTagName("window-overlay")[0].style.display = "block";
    return false;
}

function resizeInit(e)
{
    resizing = true;
    
    var obj = this.parentElement.getElementsByTagName("window-content")[0];
    //obj = this.parentElement;
    windowX = obj.offsetWidth;
    windowY = obj.offsetHeight;
    startX = e.clientX;
    startY = e.clientY;
    draggingObj = obj;
    this.parentElement.getElementsByTagName("window-overlay")[0].style.display = "block";
    return false;
}

function hideOverlay(e)
{
    if (e.clientY < 40)
    {
        this.handle.maximize();
    }
    this.handle.handle.getElementsByTagName("window-overlay")[0].style.display = "none";
}

function selectTop(e)
{
    makeTopMost(this);
}

function bringToFront(e)
{
    makeTopMost(this.handle.handle);
}

function btnMinimize(e)
{
    //e = e || window.event;
    e.stopPropagation();
    alert("minimize");     
    return false;
}

function btnMaximize(e)
{
    e.stopPropagation();
    this.handle.maximize();
    return false;
}

function btnClose(e)
{
    e.stopPropagation();
    this.handle.close();
    return false;
}