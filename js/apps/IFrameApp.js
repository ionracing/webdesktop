var IFrameApp = Object.extend(Application, function IFrameApp(url) {
    Application.call(this);
    this.url = url;
});

IFrameApp.prototype.title = "IFrame App";

IFrameApp.prototype.getContent = function(){
    return "<iframe style='width: 100%; height: 100%' src='" + this.url +  "'></iframe>";
}

//#########################################################################

var MngConsole = Object.extend(IFrameApp, function(){
    IFrameApp.call(this, "http://127.0.0.1:9966/");
});

MngConsole.prototype.title = "Management Console";
