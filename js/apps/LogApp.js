var LogApp = Object.extend(Application, function LogApp(){
    Application.call(this);
});

LogApp.prototype.title = "Log App";

LogApp.prototype.getContent = function(){
    return document.getElementById("log").innerHTML;
}

LogApp.prototype.main = function(){
    this.window.setSize(540, 250);
}