var LoremApp = Object.extend(Application, function(){
    
});

LoremApp.prototype.title = "Lorem Ipsum";

LoremApp.prototype.getContent = function(){
    return "<h2>Lorem Ipsum test</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae tristique dolor. Maecenas eros massa, imperdiet sed lacinia quis, suscipit ac nisi. Vestibulum lorem tellus, iaculis in quam ac, malesuada rutrum ex. Donec massa enim, tincidunt ac elementum ac, sagittis nec felis. Etiam faucibus, enim at pretium auctor, nulla lacus tempor lectus, non commodo nibh quam eu justo. Aenean et velit eu dolor egestas tincidunt dignissim nec tellus. Quisque nec malesuada nibh. Proin nec nibh luctus, porta mi vitae, bibendum nibh. Suspendisse potenti. Aenean nec tellus augue. Pellentesque sagittis metus nisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus rutrum lorem vitae nulla sagittis, vel fermentum orci pretium. </p>";
}