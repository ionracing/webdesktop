var ServerData = Object.extend(Application, function() {
    var _this = this;
    this.timeHandle = null;
    this.handleTimer = function(){
        requestJson(getUrl("data"), _this.response);
    };
    this.response = function(data) {
        _this.data = data;
        _this.draw(data);
    }
    
});

ServerData.prototype.title = "Server Data";

ServerData.prototype.draw = function(data){
    var mk = new HtmlHelper();
    this.content.innerHTML = "";
    
    this.content.appendChild(mk.textTag("div", "Package count: " + data.PackageCount.toString()));
    var table = mk.tag("table");
    var headerRow = mk.tag("tr");
    headerRow.appendChild(mk.textTag("th", "Sensor Name"));
    headerRow.appendChild(mk.textTag("th", "Latest Value"));
    table.appendChild(headerRow);
    var dataRow = null;
    for(var i = 0; i < data.Data.length; i++)
    {
        dataRow = mk.tag("tr");
        dataRow.appendChild(mk.textTag("td", data.Data[i].Name));
        dataRow.appendChild(mk.textTag("td", data.Data[i].Value));
        table.appendChild(dataRow);
    }
    this.content.appendChild(table);
    
}

ServerData.prototype.main = function(){
    this.timeHandle = setInterval(this.handleTimer, 1000);
}

ServerData.prototype.close = function(){
    clearInterval(this.timeHandle);
}