var ServerExplorer = Object.extend(Application, function() {
    var _this = this;
    this.response = function(data) {
        _this.data = data;
        _this.draw(data);
    }
    
    this.folderClick = function(e){
        var curPath = _this.data.CurPath.split(":").pop().replace(/\\/g, "/");
        requestJson(getUrl("explore") + curPath + this.name, _this.response);
    }
    
    this.fileClick = function(e){
        var curPath = _this.data.CurPath.split(":").pop().replace(/\\/g, "/");
        window.location = getUrl("explore") + curPath + this.name;
    }
    
});

ServerExplorer.prototype.title = "Server Explorer";

ServerExplorer.prototype.draw = function(data){
    var mk = new HtmlHelper();
    this.content.innerHTML = "";
    
    this.content.appendChild(mk.textTag("div", "Current path: " + data.CurPath));
    var genTable = new HtmlTableGen();
    genTable.setClassName("explorer clickable")
    genTable.addHeader("", "Name", "Date Modified", "Type", "Size");
    
    genTable.addRow([{"event": "click", "func": this.folderClick}, { field: "name", data: ".."}], " ", "..", "", "up", "");
    var dataRow = null;
    var line = null;
    for(var i = 0; i < data.DirectoryInfo.length; i++){
        line = data.DirectoryInfo[i];
        genTable.addRow(
            [{event: "click", func: this.folderClick}, {field: "name", data: line.Name}]
            ,'<img style="width: 16px; height: 16px;" src="/Desktop/images/folder-icon-64x64.png">'
            , line.Name
            , line.DateMod
            , line.Type
            , line.Size
             );
    }
    for(var i = 0; i < data.FileInfo.length; i++){
        line = data.FileInfo[i];
        var ext = line.Name.split(".").pop();
        genTable.addRow(
            [{event: "click", func: this.fileClick}, {field: "name", data: line.Name}]
            ,'<img style="width: 16px; height: 16px;" src="/Desktop/images/genicons/' + ext + '.png">'
            , line.Name
            , line.DateMod
            , line.Type
            , line.Size
             );
        dataRow = mk.tag("tr", [{"event": "click", "func": this.fileClick}]);
    }
    this.content.appendChild(genTable.generate());
}

ServerExplorer.prototype.main = function(){
    requestJson(getUrl("explore"), this.response);
};