var ServerInfo = Object.extend(Application, function() {
    var _this = this;
    this.response = function(data){
        var mk = new HtmlHelper();
        for(var f in data){
            _this.content.appendChild(mk.textTag("div", f + ": " + data[f]));
        }
    }
});

ServerInfo.prototype.title = "Server info";

ServerInfo.prototype.main = function(){
    requestJson(getUrl("serverinfo"), this.response);
    
}