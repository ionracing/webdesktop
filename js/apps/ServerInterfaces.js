var ServerInterfaces = Object.extend(Application, function() {
    var _this = this;

    this.handleTimer = function () {
        requestJson(getUrl("interfaces"), _this.response);
    }

    this.response = function(data){
        _this.data = data;
        _this.draw(data);
    }
});

ServerInterfaces.prototype.draw = function(data){
    var mk = new HtmlHelper();
    this.content.innerHTML = "";
    var table = new HtmlTableGen();
    table.addHeader("Name", "Total sent", "Total received");
    
    for(var i = 0; i < data.AllInterfaces.length; i++){
        var current = data.AllInterfaces[i]; 
        table.addRow(current.Name, current.SentBytes, current.ReceivedBytes);
    }
    this.content.appendChild(table.generate());
}

ServerInterfaces.prototype.title = "Server Interfaces";

ServerInterfaces.prototype.main = function(){
    this.timeHandle = setInterval(this.handleTimer, 1000);
}

ServerInterfaces.prototype.close = function(){
    clearInterval(this.timeHandle);
}