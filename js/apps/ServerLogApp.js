/**
 * @class {ServerLog}
 */
var ServerLog = Object.extend(Application, function LogApp2(){
    Application.call(this);
    this.filterValue = "";
    var _this = this;
    this.showAll = function(){
        _this.drawTable(0, _this.filterValue);
    }
    this.showInfo = function(){
        _this.drawTable(2, _this.filterValue);
    }
    this.showError = function(){
        _this.drawTable(3, _this.filterValue);
    }
    
    this.response = function(data){
        _this.data = data;
        _this.draw(2, _this.filterValue);
    };
    
    this.filter = function(e){
        //alert(e.target.value);
        _this.drawTable(_this.lastLevel, e.target.value);
        
    }
});

ServerLog.prototype.title = "Server Log App";

ServerLog.prototype.getContent = function(){
    return "";
}

ServerLog.prototype.draw = function(level, filter){
        var mk = new HtmlHelper();
        this.content.innerHTML = "";
        this.content.appendChild(mk.textTag("gui-button", "All", [{"event": "click", "func":this.showAll}]));
        this.content.appendChild(mk.textTag("gui-button", "Info", [{"event": "click", "func":this.showInfo}]));
        this.content.appendChild(mk.textTag("gui-button", "Error", [{"event": "click", "func":this.showError}]));
        var inputTag = mk.tag("input", [{"event": "input", "func": this.filter}]);
        this.tableContainer = mk.tag("div");
        inputTag.type = "text";
        //inputTag.value = filter;
        this.content.appendChild(inputTag);
        this.content.appendChild(this.tableContainer);
        this.drawTable(level, filter);
}

/**
 * This is a drawTable function
 * 
 * @param {number} level The level to use when filtering
 * @param {string} RegExp string to use when filtering
 */
ServerLog.prototype.drawTable = function(level, filter)
{
    this.lastLevel = level;
    this.filterValue = filter;
    this.tableContainer.innerHTML = "";
    var genTable = new HtmlTableGen();
    genTable.addHeader("Time", "Sender", "Level", "Info");
        
    for(var i = 0; i < this.data.Data.length; i++){
        if (this.data.Data[i].Level >= level && (filter == null || filter == "" || this.data.Data[i].Sender.match(new RegExp(filter, "i"))))
        {
            var line = this.data.Data[i];
            genTable.addRow(line.Time, line.Sender, line.Level, line.Value);
        }
    }
    this.tableContainer.appendChild(genTable.generate());
}
/**
 * This is the main entry of ServerLogger
 */
ServerLog.prototype.main = function(){
    
    requestJson(getUrl("log"), this.response);
    this.window.setSize(540, 250);
}
