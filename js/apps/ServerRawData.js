var ServerRawData = Object.extend(Application, function() {
    var _this = this;
    this.timeHandle = null;
    this.handleTimer = function(){
        requestJson(getUrl("rawdata"), _this.response);
    };
    this.response = function(data) {
        _this.data = data;
        _this.draw(data);
    }
    
});

ServerRawData.prototype.title = "Server Can Data";

ServerRawData.prototype.draw = function(data){
    var mk = new HtmlHelper();
    this.content.innerHTML = "";
    
    //this.content.appendChild(mk.textTag("div", "Package count: " + data.PackageCount.toString()));
    var table = new HtmlTableGen();
    table.addHeader("ID", "Data", "Data[0]", "Data[1]", "Data[2]", "Data[3]", "Time stamp", "Time delta (ms)", "Time avg (ms)", "count");
    for(var i = 0; i < data.Data.length; i++)
    {
        var current = data.Data[i];
        table.addRow(current.ID, current.Data, current.Data0, current.Data1, current.Data2, current.Data3, current.TimeStamp, current.TimeDelta, current.TimeAvg, current.Count)
    }
    this.content.appendChild(table.generate());
    
}

ServerRawData.prototype.main = function(){
    this.timeHandle = setInterval(this.handleTimer, 1000);
}

ServerRawData.prototype.close = function(){
    clearInterval(this.timeHandle);
}