var ServerServices = Object.extend(Application, function() {
    var _this = this;
    this.response = function(data){
        var mk = new HtmlHelper();
        var table = new HtmlTableGen();
        table.addHeader("Name", "State");
        
        for(var i = 0; i < data.Data.length; i++){
            table.addRow(data.Data[i].Name, data.Data[i].State);
        }
        _this.content.appendChild(table.generate());
    }
});

ServerServices.prototype.title = "Server services";

ServerServices.prototype.main = function(){
    requestJson(getUrl("services"), this.response);
}