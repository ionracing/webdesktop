var ServerSessions = Object.extend(Application, function() {
    var _this = this;
    this.response = function(data){
        var mk = new HtmlHelper();
        var table = new HtmlTableGen();
        _this.content.appendChild(mk.textTag("div", "Current session ID: </br>" + data.CurrentSession));
        _this.content.appendChild(mk.textTag("div", "Number of sessions: " + data.Data.length));
        table.addHeader("Session");
        
        for(var i = 0; i < data.Data.length; i++){
            table.addRow(data.Data[i].SessionID);
        }
        _this.content.appendChild(table.generate());
    }
});

ServerSessions.prototype.title = "Server Session";

ServerSessions.prototype.main = function(){
    requestJson(getUrl("sessions"), this.response);
}