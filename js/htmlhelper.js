function HtmlHelper()
{
    
}

HtmlHelper.prototype.tag = function(tag, events){
    var temp = document.createElement(tag); 
    if (events != null){ 
        for(var i = 0; i < events.length; i++) {
            temp.addEventListener(events[i].event, events[i].func);
        }
    }
    return temp;
};

HtmlHelper.prototype.textTag = function (tag, text, events){
    var temp = this.tag(tag, events);
    temp.innerHTML = text;
    return temp;
};

function HtmlTableGen()
{
    this.header = [ ];
    this.rows = [ ];
}

HtmlTableGen.prototype.setClassName = function(className)
{
    this.className = className;
}

HtmlTableGen.prototype.addHeader = function()
{
    this.header = [ ];
    for(var i = 0; i < arguments.length; i++)
    {
        this.header.push(arguments[i]);
    }
}

HtmlTableGen.prototype.addRow = function()
{   
    var row = [ ];
    for(var i = 0; i < arguments.length; i++)
    {
        row.push(arguments[i]);
    }
    this.rows.push(row);
}

HtmlTableGen.prototype.generate = function()
{
    var table = document.createElement("table");
    if (this.className != null)
    {
        table.className = this.className;
    }
    if (this.header.length > 0)
    {
        var headerRow = document.createElement("tr");
        for(var i = 0; i < this.header.length; i++)
        {
            var header = document.createElement("th");
            header.innerHTML = this.header[i];
            headerRow.appendChild(header);
            
        }
        table.appendChild(headerRow);
    }
    for(var row = 0; row < this.rows.length; row++)
    {
        var rowEle = document.createElement("tr");
        for(var col = 0; col < this.rows[row].length; col++)
        {
            if (Array.isArray(this.rows[row][col]))
            {
                for(var i = 0; i < this.rows[row][col].length; i++) {
                    if (this.rows[row][col][i].event != null)
                    {
                        rowEle.addEventListener(this.rows[row][col][i].event, this.rows[row][col][i].func);
                    }
                    else if (this.rows[row][col][i].field != null)
                    {
                        rowEle[this.rows[row][col][i].field] = this.rows[row][col][i].data;
                    }
                }
            }
            else
            {
                var colEle = document.createElement("td");
                colEle.innerHTML = this.rows[row][col];
                rowEle.appendChild(colEle);
            }
        }
        table.appendChild(rowEle);
    }
    return table;
}