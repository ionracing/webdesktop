
function mkTag(tag)
{
    return document.createElement(tag);
}

function mkTagText(tag, text)
{
    var temp = mkTag(tag);
    temp.innerHTML = text;
    //temp.appendChild(document.createTextNode(""));
    return temp;
}

function mkActionTag(tag, events, windowHandle)
{
    var temp = mkTag(tag);
    if (windowHandle != null)
    {
        temp.handle = windowHandle;
    }
    if (events != null)
    {
        for(var i = 0; i < events.length; i++)
        {
            temp.addEventListener(events[i].event, events[i].func);
        }
    }
    return temp;
}

function mkActionTagText(tag, text, events, windowHandle)
{
    var temp = mkTagText(tag, text);
    if (windowHandle != null)
    {
        temp.handle = windowHandle;
    }
    if (events != null)
    {
        for(var i = 0; i < events.length; i++)
        {
            temp.addEventListener(events[i].event, events[i].func);
        }
    }
    return temp;
}

function bind(func, _this)
{
    return function() { func.apply(_this, arguments) };
}