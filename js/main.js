var dragging = false;
var resizing = false;
var windowX = 0, windowY = 0;
var startX = 0, startY = 0;

var RESTORED = 0;
var MINIMIZED = 1;
var MAXIMIZED = 2;
var TILED = 3;

function getUrl(path){
    return "/Desktop/" + path;
}

function requestJson(url, callback)
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            callback(JSON.parse(xmlhttp.responseText));
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

Object.extend = function(base, constructor){
    constructor.prototype = Object.create(base.prototype);
    constructor.prototype.constructor = constructor;
    return constructor;
}

function globalPosStore(winX, winY, mouX, mouY)
{
    windowX = winX;
    windowY = winY;
    startX = mouX;
    startY = mouY;
}

function makeWindow(appWindow)
{
    //Main window
    var mk = new HtmlHelper();
    var app_window = mk.tag("wm-window", [ { event: "mousedown", func: appWindow.selectTopEvent }]); 
    app_window.style.top = "100px";
    app_window.style.left = "100px";
    var title_bar = mk.tag("title-bar", [ { event: "mousedown", func: appWindow.dragInit } ]);   
    
    //Window controlls
    var buttonPanel = mk.tag("title-button");
    buttonPanel.appendChild(mk.textTag("div", "&#8722;", [ { event: "mousedown", func: appWindow.minimizeEvent }]));
    buttonPanel.appendChild(mk.textTag("div", "+", [ { event: "mousedown", func: appWindow.maximizeEvent }]));
    buttonPanel.appendChild(mk.textTag("div", "X", [ { event: "mousedown", func: appWindow.closeEvent }]));
    
    //Add window parts
    title_bar.appendChild(mk.textTag("title-text", appWindow.title)); //Window title
    title_bar.appendChild(buttonPanel);
    
    //Add document parts
    var app_content = mk.tag("window-content");
    var app_wrap = mk.tag("content-wrapper");
    var app_overlay = mk.tag("window-overlay");
    app_content.appendChild(app_wrap);
    app_content.appendChild(app_overlay);
    var app_resize = mk.tag("window-resizer", [{ event: "mousedown", func: appWindow.resizeInit }] );
    
    app_window.appendChild(title_bar);
    app_window.appendChild(app_content);
    app_window.appendChild(app_resize);
    
    app_wrap.innerHTML = appWindow.content;
    return app_window;
}

//-------------------------WindowManager------------------------
var WNDFULL = 0;
var WNDLEFT = 1;
var WNDRIGHT = 2;
var WNDTOPLEFT = 3;
var WNDTOPRIGHT = 4;
var WNDBOTTOMLEFT = 5;
var WNDBOTTOMRIGHT = 6;
var WNDRESTORE = 7;

/**
 * @class
 * @param {HTMLElement} bodyNode
 */
function WindowManager(bodyNode)
{
    this.windows = [];
    this.lanchers = [];
    this.nextId = 0;
    this.body = bodyNode;
    this.focusWindow = null;
    this.mk = new HtmlHelper();
    var _this = this;
    this.documentUp = function(e){
        if (dragging)
        {
            if (e.clientY < 40 && e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDTOPLEFT);
                _this.focusWindow.y = 40;    
            }
            else if (e.clientY < 40 && window.innerWidth - e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDTOPRIGHT);
                _this.focusWindow.y = 40;    
            }
            else if (window.innerHeight - e.clientY < 40 && e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDBOTTOMLEFT);
                _this.focusWindow.y = 40;    
            }
            else if (window.innerHeight - e.clientY < 40 && window.innerWidth - e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDBOTTOMRIGHT);
                _this.focusWindow.y = 40;    
            }
            else if (e.clientY < 40)
            {
                _this.focusWindow.maximize(WNDFULL);
                _this.focusWindow.y = 40;
            }
            else if (e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDLEFT);
            }
            else if (window.innerWidth - e.clientX < 20)
            {
                _this.focusWindow.maximize(WNDRIGHT);
            }
            
        }
        
        if (_this.focusWindow)
        {
            _this.focusWindow.hideOverlay();
        }
        dragging = false;
        resizing = false;
        closeMenu();
    };
    this.documentMouseMove = function(e){
        if (dragging)
        {
            _this.focusWindow.moveHandle.style.left = (e.clientX - startX + windowX).toString() + "px";
            _this.focusWindow.moveHandle.style.top = (e.clientY - startY + windowY).toString() + "px";
        }
        else if (resizing)
        {
            _this.focusWindow.resizeHandle.style.width = (e.clientX - startX + windowX).toString() + "px";
            _this.focusWindow.resizeHandle.style.height = (e.clientY - startY + windowY).toString() + "px";
        }
    }
    document.addEventListener("mouseup", this.documentUp);
    document.addEventListener("mousemove", this.documentMouseMove);
    
};



WindowManager.prototype.makeTopMost = function(appWindow){
    if (appWindow != this.focusWindow && this.focusWindow != null)
    {
        this.focusWindow.handle.style.zIndex = 1000;
    }
    if (appWindow.state == MINIMIZED)
    {
        appWindow.minimize();
    }
    this.focusWindow = appWindow;
    this.focusWindow.handle.style.zIndex = 2000;
}

WindowManager.prototype.registerWindow = function(appWindow)
{
    this.windows.push(appWindow);
    appWindow.wm = this;
    appWindow.id = this.nextId++;
    this.body.appendChild(appWindow.handle);
    var menu = document.getElementsByTagName("menu-app-win")[0];
    var tag = this.mk.textTag("nav-window", appWindow.title, [{ "event": "mousedown", "func": appWindow.selectTopEvent}]);
    tag.winHandle = appWindow;
    appWindow.menuHandle = tag;
    menu.appendChild(tag);
    this.makeTopMost(appWindow);
    appWindow.app.main();
};

WindowManager.prototype.closeWindow = function(appWindow)
{
    this.body.removeChild(appWindow.handle);
    var arrIndex = this.windows.indexOf(appWindow);
    this.windows.splice(arrIndex, 1);
    var menu = document.getElementsByTagName("menu-app-win")[0];
    menu.removeChild(appWindow.menuHandle);
};

WindowManager.prototype.registerLancher =  function (cat, lancher){
    this.lanchers.push(lancher);
    document.getElementById("Cat_" + cat).appendChild(
            this.mk.textTag(
                "new-app-item"
                , lancher.getTitle(),[{"event": "click", "func": lancher.click}] )
            );
};

//--------------------------End WindowManager---------------------


//---------------------------AppWindow---------------------------

/**
 * @class
 * @param {Application} app
 */
function AppWindow (app)
{
    this.title = app.getTitle();
    this.content = app.getContent();
    this.app = app;
    app.window = this;
    this.wm = null;
    this.id = -1;
    this.width = 0;
    this.height = 0;
    this.x = 100;
    this.y = 100;
    this.state = RESTORED;
    this.lastState = RESTORED;
    var _this = this;
    
    this.minimizeEvent = function(e){
        e.stopPropagation();
        _this.minimize();
        return false;  
    };
    this.maximizeEvent = function (e) {
        e.stopPropagation();
        _this.maximize();
        return false;
    };
    this.closeEvent = function (e){
        e.stopPropagation();
        _this.close();
        return false;
    };
    this.selectTopEvent = function(e){
        e.stopPropagation();
        _this.wm.makeTopMost(_this);
    };
    this.dragInit = function(e){
        dragging = true;
        if (_this.state == MAXIMIZED || _this.state == TILED)
        {
            _this.y = e.clientY - 20;
            _this.x = e.clientX - _this.width / 2;
            _this.maximize(WNDRESTORE);
        }
        _this.wm.makeTopMost(_this);
        globalPosStore(_this.moveHandle.offsetLeft, _this.moveHandle.offsetTop, e.clientX, e.clientY);
        _this.showOverlay(true);
        return false;
    };
    this.resizeInit = function(e){
        resizing = true;
        globalPosStore(_this.resizeHandle.offsetWidth, _this.resizeHandle.offsetHeight, e.clientX, e.clientY);
        _this.showOverlay(true);
        return false;
    };
    this.hideOverlay = function(e){
        _this.showOverlay(false);
    };

    
    this.handle = makeWindow(this);
    this.handle.window = this;
    this.moveHandle = this.handle;
    this.resizeHandle = this.handle.getElementsByTagName("window-content")[0];
    app.content = this.handle.getElementsByTagName("content-wrapper")[0];
};

AppWindow.prototype.setState = function(s) {
    this.lastState = this.state;
    this.state = s;
}

AppWindow.prototype.showOverlay = function(show){
    this.handle.getElementsByTagName("window-overlay")[0].style.display = show ? "block" : "none";
}

AppWindow.prototype.setTitle = function(title)
{
    if (this.handle != null)
    {
        this.handle.getElementsByTagName("title-text")[0].innerHTML = title;
        this.menuHandle.innerHTML = title;
    }
};

AppWindow.prototype.minimize = function() {
    if (this.state != MINIMIZED){
        this.handle.style.display = "none";
        this.setState(MINIMIZED);
    }
    else{
        this.handle.style.display = "block";
        this.setState(this.lastState);
    }
}

AppWindow.prototype.maximize = function(state)
{
    if (this.wm != null)
    {
        var content = this.handle.getElementsByTagName("window-content")[0];
        if (this.state == RESTORED)
        {
            this.x = this.handle.offsetLeft;
            this.y = this.handle.offsetTop;
            
            
            this.width = content.offsetWidth;
            this.height = content.offsetHeight;
            var newWidth = 0;
            var newHeight = 0;
            var newX = 0;
            var newY = 0;
            var newState = MAXIMIZED;
            switch(state)
            {
                case WNDLEFT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight - 78;
                    newX = -8;
                    newY = 40;
                    newState = TILED;
                    break;
                case WNDRIGHT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight - 78;
                    newX = window.innerWidth / 2;
                    newY = 40;
                    newState = TILED;
                    break;
               case WNDTOPLEFT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight / 2 - 64;
                    newX = -8;
                    newY = 40;
                    newState = TILED;
                    break;
                case WNDTOPRIGHT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight / 2 - 64;
                    newX = window.innerWidth / 2;
                    newY = 40;
                    newState = TILED;
                    break;
                case WNDBOTTOMLEFT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight / 2 - 64;
                    newX = -8;
                    newY = 24 + window.innerHeight / 2;
                    newState = TILED;
                    break;
                case WNDBOTTOMRIGHT:
                    newWidth = window.innerWidth / 2 - 8;
                    newHeight = window.innerHeight / 2 - 64;
                    newX = window.innerWidth / 2 ;
                    newY = 24 + window.innerHeight / 2;
                    newState = TILED;
                    break;
                default:
                    newWidth = window.innerWidth;
                    newHeight = window.innerHeight - 78;
                    newX = -8;
                    newY = 40;
                    newState = MAXIMIZED;
            }
            content.style.width = newWidth.toString() + "px";
            content.style.height = newHeight.toString() + "px";
            this.handle.style.top = newY.toString() + "px";
            this.handle.style.left = newX.toString() + "px";
            this.setState(newState);
        }
        else if (this.state == TILED && (state == null || state == WNDFULL))
        {
            content.style.width = window.innerWidth.toString() + "px";
            content.style.height = (window.innerHeight - 78).toString() + "px";
            this.handle.style.top = (40).toString() + "px";
            this.handle.style.left = (-8).toString() + "px";
            this.setState(MAXIMIZED);
        }
        else
        {
            content.style.width = (this.width).toString() + "px";
            content.style.height = (this.height).toString() + "px";
            this.handle.style.top = this.y.toString() + "px";
            this.handle.style.left = this.x.toString() + "px";
            this.setState(RESTORED)
        }
    }
};
AppWindow.prototype.close = function() 
{
    if (this.app.close != null)
        this.app.close();
    if (this.wm != null)
    {
        this.wm.closeWindow(this);
    }
    else
    {
        alert("Dose not contain window manager");
    }
};
AppWindow.prototype.setLocation = function(x, y){
    this.moveHandle.style.left = x.toString() + "px";
    this.moveHandle.style.top = y.toString() + "px";
}

AppWindow.prototype.setSize = function(width, height) {
    this.resizeHandle.style.width = width.toString() + "px";
    this.resizeHandle.style.height = height.toString() + "px";
}

//---------------------------End AppWindow---------------------------


//---------------------------Start Application---------------------------

/**
 * @class {Application} Application
 */
function Application()
{
    
}

Application.getTitle = function(app){
    return app.prototype ? app.prototype.title : app.getTitle();
}

Application.prototype.title = "Undefined";

Application.prototype.getTitle = function(){
    return this.title;
}

Application.prototype.getContent = function(){
    return "<h3>" + this.title + "</h3>";
};

Application.prototype.main = function(){
    
};


//--------------------------- End Application---------------------------

//---------------------------Start Lancher---------------------------


/**
 * @class
 * @param {Application} app 
 */
function Lancher(app){
    this.App = app;
    var _this = this;
    this.click = function(){
        _this.start();
    }
}

Lancher.prototype.getTitle = function(){
    return Application.getTitle(this.App);
} 

Lancher.prototype.start = function(){
    var app = new this.App();
    if (app instanceof Application)
    {
        var temp = new AppWindow(app);
        document.wm.registerWindow(temp);
        return temp;
    }
    else{
        if (app.main){
            app.main();
        }
    }
}

